<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');

Route::post('/contacts/new', 'PublicController@contactNew')->name('contacts.new');

Route::get('/contacts/{id}', 'PublicController@contactDetail')
	->name('detail')
	->where('id', '[0-9]+');


Route::delete('/contacts/delete/{id}', 'PublicController@contactDelete')
	->name('contacts.delete')
	->where('id', '[0-9]+');

Route::get('/contacts/edit/{id}', 'PublicController@contactEdit')
	->name('edit')
	->where('id', '[0-9]+');

Route::put('/contacts/edit/{id}', 'PublicController@contactUpdate')
	->name('update')
	->where('id', '[0-9]+');

Route::post('/contacts/mail', 'PublicController@contactform')->name('mail');

Route::get('/contacts/message/{id}', 'PublicController@message')
	->name('message')
	->where('id', '[0-9]+');

Route::post('/contacts/message/{id}', 'PublicController@sendmessage')->name('sendmessage');




Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


