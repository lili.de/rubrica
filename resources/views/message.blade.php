@extends('layouts.app')

@section('content')
<div class="container">

 <br>

 <div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">Invia un messaggio a {{ $contact ['name'] }} {{ $contact ['surname'] }}</div>

      <div class="card-body">

        @include('_messageform')

      </div>
    </div>
  </div>
</div>
</div>


@endsection


