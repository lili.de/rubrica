 <form method="POST" action="{{ route('sendmessage', [$id]) }}">
  @csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-2 col-form-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="inputName" placeholder="Nome">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputSurname" class="col-sm-2 col-form-label">Cognome</label>
    <div class="col-sm-10">
      <input type="text" name="surname" class="form-control" id="inputSurname" placeholder="Cognome">
    </div>
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Messaggio</label>
    <textarea class="form-control" name="message" id="inputMessage" rows="3"></textarea>
  </div>

  <button type="submit" class="btn btn-info">Invia</button>

</form>