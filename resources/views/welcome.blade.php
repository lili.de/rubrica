@extends('layouts.app')

@section('content')
<div class="container">

@if (session('status'))
<br><br>
<div class="alert alert-{{session('status')}}" role="alert">
{{session('message')}}
</div>
<br><br>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Aggiungi un nuovo contatto in rubrica</div>

            <div class="card-body">

             @include('_form')

         </div>
     </div>
 </div>
</div>

<br><br>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Rubrica ( {{ $count }} contatti )</div>

            <div class="card-body">

                @include('_contacts')
                
            </div>
        </div>
    </div>
</div>

</div>
@endsection


