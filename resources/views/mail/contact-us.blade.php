@component('mail::message')

Ciao {{ $data['name'] }}

Grazie per la segnalazione

@component('mail::button', ['url' => 'http://localhost:8000/'])
Rubricami
@endcomponent

@component('mail::panel', ['url' => ''])
<i>Simplicity--the art of maximizing the amount of work not done--is essential</i>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
