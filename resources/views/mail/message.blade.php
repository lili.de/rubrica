<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title></title>
</head>
<body>

	<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">

		<br>

		<main role="main" class="inner cover">
			<h3 class="cover-heading">Ciao {{ $contact['name'] }}</h3>
			<br>

			<h4>{{ $data['name'] }} {{ $data['surname'] }} ti ha contattato.</h4>
			<br><br>

			Il suo messaggio è:

			<p class="lead">{{ $data['name'] }}</p>

		</main>
		<footer class="mastfoot mt-auto">
			<div class="inner">
				Thanks,<br>
				<a href="http://localhost:8000/">{{ config('app.name') }}</a>
			</div>
		</footer>
	</div>

</body>
</html>

