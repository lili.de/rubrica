@if ($count>0)

	@foreach ($contacts as $contact)

	<strong>{{ $contact['surname'] }}</strong> &nbsp;
	<i>{{ $contact['name'] }}</i> : &nbsp;{{ $contact['mobile'] }}&nbsp;
	<a href="{{ route('detail', [$loop->index])}}"><span style="font-size: 0.7rem;">DETTAGLIO</span></a>&nbsp;

	<form action=" {{ route('contacts.delete', [ $loop->index ]) }}" method="POST" style="display: inline; ">

		@method('DELETE')
		@csrf
		<button type="submit" style="font-size: 0.7rem; padding-left: 0.5px; padding-right: 0.5px; padding-bottom: 0px; padding-top: 0px;" class="btn btn-outline-danger">CANCELLA</button>

	</form>

	<a href="{{ route('edit', [ $loop->index ])}}" class="btn btn-outline-warning" style="padding-left: 0.5px; padding-right: 0.5px; padding-bottom: 0px; padding-top: 0px; font-size: 0.7rem;"><span>MODIFICA</span></a>&nbsp;

	<a href="{{ route('message', [ $loop->index ])}}" class="btn btn-outline-success" style="padding-left: 0.5px; padding-right: 0.5px; padding-bottom: 0px; padding-top: 0px; font-size: 0.7rem;"><span>CONTATTA</span></a>&nbsp; 

	<br>

	@endforeach

@endif
