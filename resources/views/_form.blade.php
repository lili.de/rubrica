 <form method="POST" action="{{ route('contacts.new') }}">
  @csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-2 col-form-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="inputName" placeholder="Nome">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputSurname" class="col-sm-2 col-form-label">Cognome</label>
    <div class="col-sm-10">
      <input type="text" name="surname" class="form-control" id="inputSurname" placeholder="Cognome">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputMobilePhone" class="col-sm-2 col-form-label">Numero Cellulare</label>
    <div class="col-sm-10">
      <input type="text" name="mobile" class="form-control" id="inputMobilePhone" placeholder="Numero Cellulare">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" name="email" class="form-control" id="inputMobilePhone" placeholder="Email">
    </div>
  </div>

  <button type="submit" class="btn btn-info">Aggiungi</button>

</form>